# Pellucida

In the 1980s Bigelow & Holmes licensed a bitmap font called
_Pellucida_.

It is quite elusive, but i have reconstructed a version from the
Plan 9 sources.


## Reconstruction in 2022

If you have a suitable selection of command lines tools
installed, you can run `sh sources/build.sh` to reconstruct vector TTF
versions of the Pellucia strikes from Plan 9 (with a limited
range of glyphs).

The required tools are:

- `priplan9topng` from my [PyPNG](https://pypi.org/project/pypng/)
- `vec8`, `ttf8`, `assfont` from my
  [font8](https://git.sr.ht/~drj/font8)

<figure>
<img style="width: 100vmin; height: 100vmin;" src="apel13M-plaque.svg"></img>
<figcaption="Showing of 13 pel" />
</figure>
<figure>
<img style="width: 100vmin; height: 100vmin;" src="apel15M-plaque.svg"></img>
<figcaption="Showing of 15 pel" />
</figure>
<figure>
<img style="width: 100vmin; height: 100vmin;" src="apel18M-plaque.svg"></img>
<figcaption="Showing of 18 pel (note ASCII only)" />
</figure>
<figure>
<img style="width: 100vmin; height: 100vmin;" src="apel25M-plaque.svg"></img>
<figcaption="Showing of 25 pel (note ASCII only)" />
</figure>


## Mentions and so on

Pellucida is mentioned in 
[a blog post explaining some of the design thinking behind the
_Lucida_ super
family](https://web.archive.org/web/20220704200847/https://lucidafonts.com/blogs/bigelow-holmes-blog/how-and-why-we-designed-lucida):

> We produced a series of bitmap fonts, which we called “Pellucida” for screen displays, including on a Smalltalk workstation and on the operating system Plan 9 from Bell Labs.

It is also mentioned once in the remix edition of 20th Century Type,
but sadly with no showing.

In what appears to be a sort of
[review of fonts that you might like to use in TeX in
1988](https://tug.org/TUGboat/tb09-2/tb21wujastyk.pdf),
Pellucida is mentioned as an
afterthought in a section that otherwise describes the Lucida
extended family.
Intriguingly the suggestion is made that the Pellucida _are_ the
pixel-based screen versions of Lucida.
_pel_ being an alternative abbreviation for _pixel_
(both short for _picture element_).


## Tektronix

Adding "smalltalk" to our Google search leads to [this PDF file
which appears to be a change set for the manuals for the
Tektronix 4405 Artificial Intelligence System Smalltalk-80
System](https://web.archive.org/web/20220704201630/http://www.bitsavers.org/pdf/tektronix/44xx/070-5606-00_4404_Smalltalk_Manual_Update_Jan87.pdf).
That document mentions that, in addition to the usual Xerox
fonts, the Tektronix workstation has Pellucida installed.

In fact the Tektronix manual mentions:
Pellucida Sans-Serif, Pellucida Serif, Pellucida Typewriter.
Pellucida Sans and Pellucida Serif are proportional fonts (many
letter widths), whereas the manual specifically explains that
Pellucida Typewriter is monospaced (one letter width).
It seems that Pellucida Typewriter is the only monospaced on
the system (the other Xerox fonts are proportional).
I guess that Smalltalk was not usually presented in a
monospaced font.

Bitmap fonts, like metal fonts, require separate designs for
different sizes, because they cannot generally be scaled.
And true to the metal-era this Tektronix manual refers to the
different designs available at different sizes as _StrikeFont_.

This Pellucida Typewriter is available in 10, 12, 16, 18 point strikes.
A "point" being 1/72 inch, but actually a pixel.
The size being the height of the capital **A**.
The proportional fonts are available in more strike sizes.
The other (Xerox) fonts are available in only two sizes: 10 and 12.
That suggests that possibly Tektronix were using Pellucida as a marketable
typographic advantage over competitors.

The character set for Pellucida Typewriter covers ASCII and an
extended 8-bit set between 0x80 and 0xFF, but it is not the now
familiar and common Latin-1 set from Unicode.
It includes a reasonable set of accented letters for Western
European languages,
a complete set of superscript numbers from 0
to 9 (unlike Latin-1),
a subset of Greek apparently intended for
writing maths but not Greek,
and a set of box drawing characters.

There do not appear to be any specimens, so we may never know
how this font looked.


## Plan 9

The other clue is that Pellucida was used in Plan 9.
This is actually how i came across it, casually mentioned in a
random Twitter thread.

The Plan 9 bitmap fonts are available in the directory
`/lib/font/bit`.
On a Plan 9 installation.

However, they are also available at
https://plan9.io/sources/plan9/lib/font/bit/ on the World Wide
Web.
In Plan 9 font format.

5 strikes are available in different sizes, so-called 8, 9, 10,
12, 16.

8 and 9 have better glyph coverage than sizes 10 12 16.
8 and 9 cover Latin-1 and some other regions, whereas 10, 12, 16
are available in ASCII only.

The vertical extents (cap heights, d-height, and so on) of the strikes are:

- latin1.8 **E** 9 pixels; **x** 7; **d** 10; **p** 10; **d/p** 13
- latin1.9 **E** 11 pixels; **x** 9; **d** 12; **p** 12; **d/p** 15
- ascii.10 **E** 11 pixels; **x** 9; **d** 12; **p** 12; **d/p** 15
- ascii.12 **E** 13 pixels; **x** 10; **d** 14; **p** 14; **d/p** 18
- ascii.16 **E** 19 pixels; **x** 14; **d** 20; **p** 19; **d/p** 25

By convention the number in the font name is supposed to be
approximately the x-height, which it sort of is; but in this
case it is the monospace width, except for `ascii.10` (width 9).

A casual inspection would suggest that `ascii.10` and `latin1.9`
have the same letterforms (of course `latin1.9` also has more
Unicode coverage).
But a more thought inspection reveals minor differences in
detail: the **j** is shifted by 1-pixel; the **w** **x** **y**
**z** have internal _serifs_ or ticks that are absent or
different in detail.

# END
