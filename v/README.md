# Vendoring

Vendored (copied) contents of a selection of the Plan 9 bitmap
fonts. Copied from a clone of the Plan 9 source repo https://github.com/plan9foundation/plan9/tree/main/lib/font/bit
with the directory structure intact:

    plan9/lib/font/bit/misc
    plan9/lib/font/bit/pelm

# END
