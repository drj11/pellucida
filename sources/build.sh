#!/bin/sh

# Following https://googlefonts.github.io/gf-guide/upstream.html
# this should be sources/build.sh

set -e

# Final filename is apelSomething
mk () {
    S=$1
    shift
    F=v/plan9/lib/font/bit/pelm/$1
    C=control/control-apel${S}M.csv
    D=apel${S}M

    priplan9topng --font "$F" |
    vec8 -control "$C" -scale 1:1 - |
    ttf8 -dir "$D" -control "$C" -

    f8name -dir "$D" -control "$C"

    assfont -dir "$D"
    mv "$D".ttf fonts/ttf/
}

mkdir -p fonts/ttf


# First arg is component of final font file name
# and here by convention the em height which is set
# to distance between d's ascender and p's descender.
mk 13 latin1.8
mk 15 latin1.9
mk a15 ascii.10
mk 18 ascii.12
mk 25 ascii.16
